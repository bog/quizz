#!/bin/sh

BASENAME=""

if [ ! -z "$1" ]
then
    BASENAME="$1"
else
    BASENAME="photo"
fi

NAME="$BASENAME.png"

echo $NAME

adb shell screencap -p "/sdcard/$NAME"
adb pull "/sdcard/$NAME"
adb shell rm "/sdcard/$NAME"

package ufrst.samp.bog;

// Java
import java.util.ArrayList;
// Android
import android.util.Log;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.content.DialogInterface;
import android.content.Intent;
// Widget
import android.widget.Toast;
import android.widget.Button;
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
// MVC
import ufrst.samp.bog.model.*;

/**
 * Main activity of the application.
 * @author bog
 */
public class MainActivity extends Activity
{
    private ListView quiz_list;

    /** Called when the activity is first created. */
    @Override
    public void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

	this.quiz_list = (ListView) findViewById(R.id.quiz_list);
    }

    @Override
    public void onResume ()
    {
	super.onResume();
	App.getInstance(this).reloadQuizzes();
	initializeQuizList();
    }

    /**
     * Fill the quiz list with all the quiz of the game.
     */
    private void initializeQuizList()
    {
	ArrayAdapter<Quiz> adapter;
	adapter = new ArrayAdapter<Quiz>( this
					  , android.R.layout.simple_list_item_1
					  , App.getInstance(this).getAllQuiz() );

	this.quiz_list.setAdapter(adapter);

	this.quiz_list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
		@Override
		public void onItemClick(AdapterView adapter, View view, int position, long id)
		{
		    final Dialog dialog = new Dialog(MainActivity.this);
		    final Quiz quiz = App.getInstance(MainActivity.this).getQuiz(position);

		    dialog.setContentView(R.layout.selectquiz);
		    dialog.setTitle(R.string.app_name);
		    
 		    TextView title = (TextView) dialog.findViewById(R.id.selectQuiz_title);
		    TextView score = (TextView) dialog.findViewById(R.id.selectQuiz_score);
		    Button play = (Button) dialog.findViewById(R.id.selectQuiz_play);
		    Button edit = (Button) dialog.findViewById(R.id.selectQuiz_edit);
		    Button delete = (Button) dialog.findViewById(R.id.selectQuiz_delete);
		    Button back = (Button) dialog.findViewById(R.id.selectQuiz_back);

		    title.setText(quiz.getTitle());
		    score.setText( getResources().getString(R.string.selectQuiz_score)
				   + " : "
				   + quiz.getScore()
				   + "%");
		    
		    play.setOnClickListener(new View.OnClickListener(){
			    @Override
			    public void onClick (View view)
			    {
				playQuiz(quiz.getId());
				dialog.dismiss();
			    }
			});

		    edit.setOnClickListener(new View.OnClickListener(){
			    @Override
			    public void onClick (View view)
			    {
				AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
				
				adb.setTitle(R.string.editQuiz_warningTitleBeforeEdit);
				adb.setMessage(R.string.editQuiz_warningMsgBeforeEdit);
				adb.setIcon(R.drawable.ic_launcher);
				
				adb.setPositiveButton(R.string.editButton, new DialogInterface.OnClickListener(){
					@Override
					public void onClick (DialogInterface dialog, int id)
					{
					    editQuiz(quiz.getId());
					    dialog.dismiss();
					}
				    });

				adb.setNegativeButton(R.string.backButton, new DialogInterface.OnClickListener(){
					@Override
					public void onClick (DialogInterface dialog, int id)
					{					    
					}
				    });
				
				adb.show();
			    }
			});
		    
		    delete.setOnClickListener(new View.OnClickListener(){
			    @Override
			    public void onClick (View view)
			    {
				AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
				adb.setTitle(R.string.deleteQuiz_name);
				adb.setMessage(R.string.deleteQuiz_msg);
				adb.setIcon(R.drawable.ic_launcher);
				
				adb.setPositiveButton(R.string.deleteQuiz_delete, new DialogInterface.OnClickListener(){
					@Override
					public void onClick (DialogInterface alert_dialog, int id)
					{
					    Quiz quiz_to_delete = App.getInstance(MainActivity.this).getQuizById(quiz.getId());
					    if (quiz_to_delete == null) {return;}
					    
					    App.getInstance(MainActivity.this).deleteQuiz(quiz_to_delete);
					    initializeQuizList();
					    dialog.dismiss();
					}
				    });

				adb.setNegativeButton(R.string.deleteQuiz_back, new DialogInterface.OnClickListener(){
					@Override
					public void onClick (DialogInterface dialog, int id)
					{
					    
					}
				    });
				adb.show();
			    }
			});
		    
		    back.setOnClickListener(new View.OnClickListener(){
			    @Override
			    public void onClick (View view)
			    {
				dialog.dismiss();
			    }
			});
		    
		    dialog.show();
		}
	    });
    }

    public void onCreateQuizButtonClick(View view)
    {
	AlertDialog.Builder adb = new AlertDialog.Builder(this);
	final View dialog_view = getLayoutInflater().inflate(R.layout.createquiz, null);

	adb.setTitle(R.string.createQuiz_name);
	adb.setMessage(R.string.createQuiz_help);
	adb.setView(dialog_view);
	adb.setIcon(R.drawable.ic_launcher);
	
	adb.setPositiveButton(R.string.createQuiz_createButton, new DialogInterface.OnClickListener(){
		@Override
		public void onClick (DialogInterface dialog, int id)
		{
		    createQuiz(dialog_view);
		}
	    });

	adb.setNegativeButton(R.string.createQuiz_backButton, new DialogInterface.OnClickListener(){
		@Override
		public void onClick (DialogInterface dialog, int id)
		{
		}
	    });

	adb.show();
    }

    /**
     * Create a quiz.
     * @param View dialog_view, the view of the dialog from where the
     * title of the quiz to create where taken.
     */
    private void createQuiz (View dialog_view)
    {
	EditText createQuiz_edit = (EditText) dialog_view.findViewById(R.id.createQuiz_edit);

	// Error if no title is given.
	if ( createQuiz_edit.getText().toString().equals("") )
	    {
		Toast.makeText(this, R.string.createQuiz_noTitleError,Toast.LENGTH_SHORT).show();
		return;
	    }

	Quiz quiz = new Quiz( createQuiz_edit.getText().toString().trim() );
	App.getInstance(this).addQuiz(quiz);
	editQuiz(quiz.getId());
    }

    /**
     * Play a quiz.
     * @param int id, the id of the quiz to play.
     */
    private void playQuiz (int id)
    {
	Quiz quiz = App.getInstance(this).getQuizById(id);
	if (quiz == null) { return; }
	
	Intent intent = new Intent(this, PlayQuizActivity.class);
	intent.putExtra( "quiz", App.getInstance(this).indexOfQuiz(quiz) );
	startActivity(intent);
    }

    /**
     * Edit a quiz.
     * @param int id, the id of the quiz to edit.
     */
    private void editQuiz (int id)
    {
	Quiz quiz = App.getInstance(this).getQuizById(id);

	if (quiz == null) { return; }
	
	// Lose the score to avoid cheating.
	quiz.setScore(0);
	
	Intent intent = new Intent(this, EditQuizActivity.class);
	intent.putExtra( "quiz", App.getInstance(this).indexOfQuiz(quiz) );
	startActivity(intent);
    }

    public void onGetMoreQuizButtonClick (View view)
    {
	Intent intent = new Intent(this, MoreQuizActivity.class);
	startActivity(intent);
    }

}

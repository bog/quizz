package ufrst.samp.bog;

// Java
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
// Android
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.content.DialogInterface;
// Widget
import android.widget.Toast;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
// MVC
import ufrst.samp.bog.model.*;

/**
 * Allow the user to download new quizzes.
 * @author bog
 */
public class MoreQuizActivity extends Activity
{

    private ListView list;
    private ArrayList<Quiz> new_quizzes;
    
    @Override
    public void onCreate (Bundle bundle)
    {
	super.onCreate(bundle);
	setContentView(R.layout.morequiz);

	this.list = (ListView) findViewById(R.id.moreQuiz_list);
	this.new_quizzes = new ArrayList<Quiz>();
    }

    @Override
    public void onResume ()
    {
	super.onResume();
	initializeNewQuizzes();
	initializeList();
    }

    /**
     * Initialize the array of quizzes that can be downloaded by the
     * user.
     */
    private void initializeNewQuizzes ()
    {
	this.new_quizzes = new ArrayList<Quiz>();
	
	try
	    {
		XMLQuizFactory factory = new XMLQuizFactory(XMLQuizFactory.DefaultURL);
		
		Quiz quiz = null;

		do
		    {
			quiz = factory.newQuiz();
			
			if (quiz != null)
			    {
				this.new_quizzes.add(quiz);
			    }
		    }
		while (quiz != null);
	    }
	catch (Exception e)
	    {
		String err_msg = getResources().getString(R.string.moreQuiz_cannotDownloadError);
		Toast.makeText(this, err_msg, Toast.LENGTH_SHORT).show();
		return;
	    }

	// Sort all the quizzes.
	Collections.sort( this.new_quizzes, new Comparator<Quiz>(){
		@Override
		public int compare (Quiz q1, Quiz q2)
		{
		    return q1.getTitle().compareTo( q2.getTitle() );
		}
	    });
    }
    
    /**
     * Initialize the list of quizzes that can be downloaded by the
     * user.
     */
    private void initializeList ()
    {
	ArrayAdapter<Quiz> adapter;
	adapter = new ArrayAdapter<Quiz>(this
					 , android.R.layout.simple_list_item_1
					 , this.new_quizzes);
	this.list.setAdapter(adapter);

	this.list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
		@Override
		public void onItemClick (AdapterView adapter, View view, int position, long id)
		{
		    final Quiz quiz = MoreQuizActivity.this.new_quizzes.get(position);

		    AlertDialog.Builder adb = new AlertDialog.Builder(MoreQuizActivity.this);
		    adb.setTitle(R.string.downloadQuiz_name);
		    
		    adb.setMessage( getResources().getString(R.string.downloadQuiz_msg1)
				    + " " + quiz.getTitle() + " "
				    + getResources().getString(R.string.downloadQuiz_msg2) );
		    
		    adb.setIcon(R.drawable.ic_launcher);
		    
		    adb.setPositiveButton(R.string.downloadQuiz_downloadButton, new DialogInterface.OnClickListener(){
			    @Override
			    public void onClick (DialogInterface dialog, int id)
			    {
				App.getInstance(MoreQuizActivity.this).addQuiz(quiz);
				App.getInstance(MoreQuizActivity.this).saveQuiz(quiz);

				initializeNewQuizzes();
				initializeList();

				// Download succes : user feedback dialog.
				AlertDialog.Builder adb = new AlertDialog.Builder(MoreQuizActivity.this);
				
				adb.setTitle(R.string.downloadQuiz_name);
				adb.setMessage(R.string.downloadQuiz_success);				
				adb.setIcon(R.drawable.ic_launcher);
				
				adb.setPositiveButton(R.string.backButton, new DialogInterface.OnClickListener(){
					@Override
					public void onClick (DialogInterface dialog, int id)
					{
					}
				    });
				
				adb.show();
			    }
			});

		    adb.setNegativeButton(R.string.backButton, new DialogInterface.OnClickListener(){
			    @Override
			    public void onClick (DialogInterface dialog, int id)
			    {
			    }
			});
		    
		    adb.show();
		}
	    });
    }

    public void onMoreQuizBackButton (View view)
    {
	finish();
    }
}

package ufrst.samp.bog.model;

// Java
import java.util.ArrayList;
// Android
import android.content.Context;
import android.content.ContentValues;
// Database
import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;

/**
 * Interface between the quiz model and the quiz database.
 * @author bog
 */
public class QuizDatabase
{
    private QuizDatabaseOpenHelper dbhelper;

    public QuizDatabase (Context context)
    {
	this.dbhelper = new QuizDatabaseOpenHelper(context);
    }

    /**
     * @param String table, the table to query.
     * @return int, the id of the last item of the table or -1 if no
     * item was found.
     */
    public int getLastId(String table)
    {
	SQLiteDatabase db = this.dbhelper.getReadableDatabase();
	Cursor cursor;
	cursor = db.query(table
			  , new String[]{"max(id)"}
			  , null
			  , null
			  , null
			  , null
			  , null);

	if ( !cursor.moveToFirst() ) { return -1; }

        int result = cursor.getInt(QuizDatabaseOpenHelper.QuizId);
	cursor.close();

	return result;
    }

    /**
     * Save a quiz into the database.
     * @param Quiz quiz, the quiz to save into the database.
     */
    public void addQuiz (Quiz quiz)
    {
	SQLiteDatabase db = this.dbhelper.getWritableDatabase();

	// Adding quiz.
	{
	    ContentValues quiz_values = new ContentValues();
	    quiz_values.put( "title", quiz.getTitle() );
	    quiz_values.put( "score", quiz.getScore() );
	    db.insert(QuizDatabaseOpenHelper.QuizTable, null, quiz_values);
	}

	int quiz_id = getLastId(QuizDatabaseOpenHelper.QuizTable);
	
	quiz.setId(quiz_id);
	
	for (Question question : quiz)
	    {
		// Adding question
		{
		    ContentValues question_values = new ContentValues();
		    question_values.put( QuizDatabaseOpenHelper.QuizTable, quiz_id );
		    question_values.put( "text", question.getText() );
		    db.insert(QuizDatabaseOpenHelper.QuestionTable, null, question_values);
		}
		int question_id = getLastId(QuizDatabaseOpenHelper.QuestionTable);
		int num = 0;

		for (String proposition : question)
		    {
			int is_answer;
			if (question.getAnswer() != null && question.getAnswer().intValue() == num) { is_answer = 1; }
			else { is_answer = 0; }

			ContentValues proposition_values = new ContentValues();
			proposition_values.put( QuizDatabaseOpenHelper.QuestionTable, question_id);
			proposition_values.put( "text", proposition );
			proposition_values.put( "isanswer", is_answer );
			db.insert(QuizDatabaseOpenHelper.PropositionTable, null, proposition_values);

			num++;
		    }
	    }
    }

    /**
     * To obtain all the quiz from the database into an array list of
     * quiz.
     * @return ArrayList<Quiz>, all the quiz from the database.
     */
    public ArrayList<Quiz> getAllQuiz()
    {
	ArrayList<Quiz> all_quiz = new ArrayList<Quiz>();
	SQLiteDatabase db = this.dbhelper.getReadableDatabase();

	Cursor quiz_cursor = db.query(QuizDatabaseOpenHelper.QuizTable
				      , null
				      , null
				      , null
				      , null
				      , null
				      , null);

	if ( !quiz_cursor.moveToFirst() )
	    {
		return all_quiz;
	    }

	do
	    {
		int quiz_id  = quiz_cursor.getInt(QuizDatabaseOpenHelper.QuizId);
		Quiz quiz = new Quiz( quiz_cursor.getString(QuizDatabaseOpenHelper.QuizTitle), quiz_id );
		quiz.setScore( quiz_cursor.getInt(QuizDatabaseOpenHelper.QuizScore) );
		
		getAllQuestions(db, quiz_id, quiz);

		all_quiz.add(quiz);
	    }
	while ( quiz_cursor.moveToNext() );

	quiz_cursor.close();
	
	return all_quiz;
    }

    /**
     * Get all the questions related to a quiz from the
     * database into an instance of a quiz.
     * @param SQLiteDatabase db, the database where to extract the
     * quiz's questions.
     * @param Quiz quiz, the quiz where to add the
     * database questions.
     * @param int quiz_id, the id of the quiz into the database.
     */
    private void getAllQuestions (SQLiteDatabase db, int quiz_id, Quiz quiz)
    {
	Cursor question_cursor = db.query(QuizDatabaseOpenHelper.QuestionTable
					  , null
					  , QuizDatabaseOpenHelper.QuizTable + "=?"
					  , new String[] {""+quiz_id}
					  , null
					  , null
					  , null);

	if (question_cursor.moveToFirst())
	    {
		do
		    {
			String question_text = question_cursor.getString(QuizDatabaseOpenHelper.QuestionText);
			Question question = new Question(question_text);
			int question_id = question_cursor.getInt(QuizDatabaseOpenHelper.QuestionId);

			getAllPropositions(db, question_id, question);

			quiz.addQuestion(question);
		    }
		while ( question_cursor.moveToNext() );
	    }

	question_cursor.close();
    }

    /**
     * Get all the propositions related to a question from the
     * database into an instance of a question.
     * @param SQLiteDatabase db, the database where to extract the
     * question's propositions.
     * @param Question question, the question where to add the
     * database propositions.
     * @param int question_id, the id of the question into the database.
     */
    private void getAllPropositions(SQLiteDatabase db, int question_id, Question question)
    {
	Cursor proposition_cursor = db.query(QuizDatabaseOpenHelper.PropositionTable
					     , null
					     , QuizDatabaseOpenHelper.QuestionTable + "=?"
					     , new String[] {""+question_id}
					     , null
					     , null
					     , null);

	if ( proposition_cursor.moveToFirst() )
	    {
		do
		    {
			String proposition_text = proposition_cursor.getString(QuizDatabaseOpenHelper.PropositionText);
			int isanswer_int = proposition_cursor.getInt(QuizDatabaseOpenHelper.PropositionIsAnswer);
			boolean isanswer = (isanswer_int == 1);

			question.addProposition(proposition_text);

			if (isanswer == true)
			    {
				question.setAnswer( question.getPropositionNumber() - 1 );
			    }
		    }
		while ( proposition_cursor.moveToNext() );
	    }

	proposition_cursor.close();
    }

    /**
     * Remove a given quiz from the database.
     * @param Quiz quiz, the quiz to remove.
     * @return boolean, weither or not the quiz has been deleted.
     */
    public boolean deleteQuiz (Quiz quiz)
    {
	if (quiz.getId() == -1) { return false; }

	SQLiteDatabase db = this.dbhelper.getWritableDatabase();

       	    
	boolean result = db.delete(QuizDatabaseOpenHelper.QuizTable
				   , "id=?"
				   , new String[]{quiz.getId()+""}) != 0;

	return result;
    }
}

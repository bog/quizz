package ufrst.samp.bog.model;

// Java
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.IOException;
// PullParser
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/**
 * Construct a quiz using a XML file from network.
 * @author bog
 */
public class XMLQuizFactory implements QuizFactory
{
    public static final String DefaultURL = "https://dept-info.univ-fcomte.fr/joomla/images/CR0700/Quizzs.xml";
    private String http_adresse;
    private XmlPullParser parser;
    
    /**
     * @param String http_adresse, String representing the URL of the
     * XML over the network.
     */
    public XMLQuizFactory (String http_adresse)
	throws Exception
    {
	this.http_adresse = http_adresse;

	XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        this.parser = factory.newPullParser();
	this.parser.setInput( getReader(http_adresse) );
    }

    /**
     * @return Quiz, the built quiz or null if no more quiz can be
     * construct from the XML.
     */
    @Override
    public Quiz newQuiz()
	throws Exception
    {
	int event_type = this.parser.getEventType();
	Quiz quiz = null;

	// Reaching the first quiz.
	while ( ( event_type != XmlPullParser.START_TAG
		  || !this.parser.getName().equals("Quizz") )
		&& event_type != XmlPullParser.END_DOCUMENT )
	    {
		event_type = this.parser.next();
	    }

	// No more quiz.
	if (event_type == XmlPullParser.END_DOCUMENT)
	    {
		return null;
	    }
	
	quiz = parseQuiz();
	
	return quiz;
    }

    /**
     * Parse an XML file to build a quiz.
     * @return Quiz, the built quiz.
     */
    private Quiz parseQuiz ()
	throws Exception
    {
	String title = this.parser.getAttributeValue(null, "type");
	if (title == null) { title = ""; }
	
	Quiz quiz = new Quiz(title);

	int event_type = this.parser.getEventType();
	
	while (event_type != XmlPullParser.END_TAG
	       || !this.parser.getName().equals("Quizz") )
	    {
		if (event_type == XmlPullParser.START_TAG)
		    {
			if (this.parser.getName().equals("Question") )
			    {
				Question question;
				question = parseQuestion();
				quiz.addQuestion(question);
			    }
		    }
		
		event_type = this.parser.next();
	    }
	
	return quiz;
    }

    /**
     * Parse an XML file to build a question.
     * @return Question, the built question.
     */
    private Question parseQuestion ()
	throws Exception
    {
	int event_type = this.parser.next();
	String text = "";
	Question question = null;

	// Looking for the question text.
	while ( event_type != XmlPullParser.START_TAG
	       || !this.parser.getName().equals("Propositions") )
	    {
		if (event_type == XmlPullParser.TEXT)
		    {
			text += this.parser.getText();
		    }
		
		event_type = this.parser.next();
	    }
	
	question = new Question( text.trim() );

	// Looking for propositions (:p).
	String proposition = "";
	while ( event_type != XmlPullParser.END_TAG
		|| !this.parser.getName().equals("Propositions") )
	    {
		if ( event_type == XmlPullParser.TEXT )
		    {
			proposition += this.parser.getText();
		    }
		
		if ( event_type == XmlPullParser.END_TAG
		     && this.parser.getName().equals("Proposition") )
		    {
			question.addProposition( proposition.trim() );
			proposition = "";
		    }
		
		event_type = this.parser.next();
	    }

	// Reach the answer number.
	while ( event_type != XmlPullParser.START_TAG
		|| !this.parser.getName().equals("Reponse") )
	    {
		event_type = this.parser.next();
	    }
	
	int answer = Integer.parseInt( this.parser.getAttributeValue(null, "valeur") );
	question.setAnswer(answer - 1);
	
	// Reach the end of the question
	while ( event_type != XmlPullParser.END_TAG
		|| !this.parser.getName().equals("Question") )
	    {
		event_type = this.parser.next();
	    }
	
	return question;
    }
    
    /**
     * Get a reader on the XML file.
     * @param String http_adresse, the http adresse to the XML file.
     * @return Reader, a reader on the XML file.
     */
    private Reader getReader (String http_adresse)
	throws IOException, MalformedURLException
    {
	return new InputStreamReader( getInputStream(http_adresse) );
    }
    
    /**
     * Get an input stream to the XML file.
     * @param String http_adresse, the http adresse to the XML file.
     * @return InputStream, an input stream to the XML file.
     */
    private InputStream getInputStream (String http_adresse)
	throws IOException, MalformedURLException
    {
	return connect(http_adresse).getInputStream();
    }
    
    /**
     * Make connection to the XML file.
     * @param String http_adresse, the http adresse to the XML file.
     * @return HttpURLConnection, the opened connection to the XML file.
     */
    private HttpURLConnection connect (String http_adresse)
	throws IOException, MalformedURLException
    {
	return (HttpURLConnection) makeURL(http_adresse).openConnection();
    }
    
    /**
     * Make an URL from an http adresse.
     * @param String http_adresse, the http adresse to the XML file.
     * @return URL, the corresponding URL.
     */
    private URL makeURL (String http_adresse)
	throws MalformedURLException
    {
        return new URL(http_adresse);
    }            
}

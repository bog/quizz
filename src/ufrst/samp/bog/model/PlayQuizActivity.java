package ufrst.samp.bog;

// Java
import java.util.Iterator;
// Android
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.content.DialogInterface;
// Widget
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ArrayAdapter;
import android.widget.Button;
//MVC
import ufrst.samp.bog.model.*;

public class PlayQuizActivity extends Activity
{
    private Quiz quiz;
    private Iterator<Question> quiz_iterator;
    private Question current_question;

    private ListView proposition_list;
    private TextView question_text;
    private Button next_button;

    private int good_choices;
    private int current_choice;

    @Override
    public void onCreate (Bundle bundle)
    {
	super.onCreate(bundle);
	setContentView(R.layout.playquiz);

	// Set the quiz to play.
	{
	    int quiz_index = getIntent().getIntExtra("quiz", -1);
	    this.quiz = App.getInstance(this).getQuiz(quiz_index);
	}

	initializeGame();
	moveToNext();
    }

    /**
     * Initialize the game.
     */
    private void initializeGame ()
    {
	this.quiz_iterator = this.quiz.iterator();
	this.proposition_list = (ListView) findViewById(R.id.playQuiz_propositionList);
	this.question_text = (TextView) findViewById(R.id.playQuiz_questionText);
	this.next_button = (Button) findViewById(R.id.playQuiz_nextButton);
	this.next_button.setEnabled(false);

	this.current_question = null;
	this.good_choices = 0;
	this.current_choice = -1;
    }
    
    /**
     * Set the current in the view.
     */
    private void initializeCurrentQuestion ()
    {
	// Set the text of the quiz.
	this.question_text.setText( this.current_question.getText() );

	// Fill the list with the proposition.
	ArrayAdapter<String> adapter;
	adapter = new ArrayAdapter<String>( this
					      , android.R.layout.simple_list_item_single_choice
					      , this.current_question.getPropositions() );
	this.proposition_list.setAdapter(adapter);
	this.proposition_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

	this.proposition_list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
		@Override
		public void onItemClick (AdapterView adapter, View view, int position, long id)
		{
		    PlayQuizActivity.this.proposition_list.setSelection(position);
		    PlayQuizActivity.this.current_choice = position;
		    PlayQuizActivity.this.next_button.setEnabled(true);
		}
	    });
    }

    /**
     * Move to the next question or end the quiz.
     */
    private void moveToNext ()
    {
	// End of the quiz.
	if (this.quiz_iterator.hasNext() == false)
	    {
		endOfQuiz();
		return;
	    }

	// Next question
	this.current_question = this.quiz_iterator.next();
	initializeCurrentQuestion();
    }

    public void onPlayQuizNextButtonClick (View view)
    {
	if (this.current_question.getAnswer() != null
	    && this.current_question.getAnswer().intValue() == this.current_choice)
	    // Right answer.
	    {
		this.good_choices++;
	    }
	else
	    // Wrong answer.
	    {
	    }

        this.next_button.setEnabled(false);
	moveToNext();
    }

    public void onPlayQuizBackButton (View view)
    {
	finish();
    }

    // Called when the quiz is done.
    public void endOfQuiz ()
    {
	int question_number = this.quiz.getQuestionNumber();
	int score = (int)(100 * this.good_choices/(float)question_number);
	boolean best_score = false;
	
	if ( score > this.quiz.getScore() )
	    {
		this.quiz.setScore(score);
		App.getInstance(this).saveQuiz(this.quiz);;
		best_score = true;
	    }
	
	AlertDialog.Builder adb = new AlertDialog.Builder(this);
	adb.setTitle( getResources().getString(R.string.endQuiz_name) );
	String msg = getResources().getString(R.string.endQuiz_msg) + " " + score + " %";

	if (best_score) { msg += "\n" +getResources().getString(R.string.endQuiz_bestScoreReached); }
	
	adb.setMessage(msg);
	adb.setIcon(R.drawable.ic_launcher);
	adb.setCancelable(false);
	
	adb.setPositiveButton(R.string.endQuiz_replayButton, new DialogInterface.OnClickListener(){
		@Override
		public void onClick(DialogInterface dialog, int id)
		{
		    initializeGame();
		    moveToNext();
		}
	    });

	adb.setNegativeButton(R.string.endQuiz_stopButton, new DialogInterface.OnClickListener(){
		@Override
		public void onClick(DialogInterface dialog, int id)
		{
		    PlayQuizActivity.this.finish();
		}
	    });
		
	adb.show();
    }
}

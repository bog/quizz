package ufrst.samp.bog.model;

// Java
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
// Android
import android.content.Context;

/**
 * Model of the quiz application.
 * @author bog
 * @note This class is a singleton.
 */
public class App
{
    // Singleton part.
    private static App instance = null;
    
    public static App getInstance (Context context)
    {
	if (App.instance == null)
	    {
		App.instance = new App(context);
	    }
		
	return App.instance;
    }
    
    // App part.
    private QuizDatabase database;
    private ArrayList<Quiz> quizzes;
    
    private App (Context context)
    {
	this.database = new QuizDatabase(context);
	reloadQuizzes();
    }

    /**
     * Reload all the quizzes of the database into the model.
     */
    public void reloadQuizzes ()
    {
	this.quizzes = new ArrayList<Quiz>();
	loadAllQuizFromDatabase();
	sortQuizzes();
    }
    
    /**
     * Add a quiz into the model.
     * @param Quiz quiz, the quiz to add.
     */
    public void addQuiz (Quiz quiz)
    {
	this.quizzes.add(quiz);
	sortQuizzes();
    }
    
    /**
     * Add or update a quiz into the database.
     * @param Quiz quiz, the quiz to add.
     */
    public void saveQuiz (Quiz quiz)
    {
	this.database.deleteQuiz(quiz);
	this.database.addQuiz(quiz);
    }

    /**
     * Give the index of a quiz.
     * @param Quiz quiz, the quiz we are looking for.
     * @return int, the index of quiz in the quizzes array or -1 if
     * the quiz is not found.
     */
    public int indexOfQuiz(Quiz quiz)
    {
	return this.quizzes.indexOf (quiz);
    }

    /**
     * Sort the quiz list lexicographically using their titles.
     */
    private void sortQuizzes ()
    {
	Collections.sort(this.quizzes, new Comparator<Quiz>(){
		@Override
		public int compare (Quiz q1, Quiz q2)
		{
		    return q1.getTitle().compareTo( q2.getTitle() );
		}
	    });
    }
    
    /**
     * Load all the quiz from the database.
     */
    public void loadAllQuizFromDatabase ()
    {
	this.quizzes = this.database.getAllQuiz();
    }

    /**
     * Remove a quiz from the model.
     * @param Quiz quiz, the quiz to remove.
     */
    public void removeQuiz (Quiz quiz)
    {
	this.quizzes.remove(quiz);
    }

    /**
     * Delete a quiz from the model and from the database.
     * @param Quiz quiz, the quiz to delete.
     */
    public void deleteQuiz (Quiz quiz)
    {
	removeQuiz(quiz);
	this.database.deleteQuiz(quiz);
    }
    
    /**
     * Delete all the quiz from the database and from the model.
     */
    public void deleteAllQuiz ()
    {
	for (Quiz quiz : this.quizzes)
	    {
		this.database.deleteQuiz(quiz);
	    }

	this.quizzes = new ArrayList<Quiz>();
    }

    /**
     * Find the first quiz with a given id.
     * @param int id, The id we are looking for.
     * @return Quiz, the first quiz with this id, or null.
     */
    public Quiz getQuizById (int id)
    {
	for ( Quiz q : getAllQuiz() )
	    {
		if (q.getId() == id)
		    {
			return q;
		    }
	    }
	
	return null;
    }
    
    // Getters and setters.
    public Quiz getQuiz (int index)
    {
	return this.quizzes.get(index);
    }

    public ArrayList<Quiz> getAllQuiz ()
    {
	return this.quizzes;
    }

    public QuizDatabase getDatabase ()
    {
	return this.database;
    }
}

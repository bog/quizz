package ufrst.samp.bog.model;

/**
 * Construct a quiz.
 * @author bog
 */
public interface QuizFactory
{
    /**
     * @return Quiz, a new quiz built by the factory.
     * @throws Exception, if the creation of a new quiz is not
     * possible.
     */
    abstract Quiz newQuiz() throws Exception;
}

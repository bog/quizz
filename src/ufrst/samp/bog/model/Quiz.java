package ufrst.samp.bog.model;

//  Java
import java.util.ArrayList;
import java.util.Iterator;

/**
 * A quiz that contain some questions.
 * @note The id of a quiz correspond to it's id on the database. If
 * the id is -1, that means that the quiz were not taken from a
 * database.
 * @author bog
 */
public class Quiz implements Iterable<Question>
{
    private String title;
    private int score;
    private ArrayList<Question> questions;
    private int id;

    /**
     * @param String title, the title of the quiz.
     */
    public Quiz (String title)
    {
	this (title, -1);
    }
    
    /**
     * @param String title, the title of the quiz.
     * @param int id, unique id of the quiz or -1.
     */
    public Quiz (String title, int id)
    {
	this.id = id;
	this.title = title;
	this.questions = new ArrayList<Question>();
	this.score = 0;
    }

    /**
     * Add a question into the quiz.
     * @param Question question, the question to add into the quiz.
     */
    public void addQuestion (Question question)
    {
	this.questions.add(question);
    }

    /**
     * Give all the questions within this quiz.
     * @return ArrayList<Question>, all the questions.
     */
    public ArrayList<Question> getAllQuestions ()
    {
	return this.questions;
    }

    /**
     * Give the index of a given question within this quiz.
     * @param Question question, the question of this quiz.
     * @return int, the index of the question within this quiz.
     */
    public int indexOfQuestion (Question question)
    {
	return this.questions.indexOf(question);
    }

    /**
     * Remove a question from this quiz.
     * @param int index, index of the question to remove.
     */
    public void removeQuestion (int index)
    {
	this.questions.remove(index);
    }
    
    @Override
    public Iterator<Question> iterator ()
    {
	return this.questions.listIterator();
    }
	
    @Override
    public String toString ()
    {
	return this.title;
    }
    
    /* Getters and setters */
    public int getId ()
    {
	return this.id;
    }

    public void setId (int id)
    {
	this.id = id;
    }
    
    public String getTitle ()
    {
	return this.title;
    }

    public void setTitle (String title)
    {
	this.title = title;
    }

    public int getScore ()
    {
	return this.score;
    }

    public void setScore (int score)
    {
	this.score = score;
    }
    
    public Question getQuestion (int index)
    {
	return this.questions.get(index);
    }    

    /**
     * @return int, how many questions there is in the quiz.
     */
    public int getQuestionNumber ()
    {
	return this.questions.size();
    }
}

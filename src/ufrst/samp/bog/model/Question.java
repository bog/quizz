package ufrst.samp.bog.model;

// Java
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Question of a quizz that contain different 
 * proposition.
 * @note Only one proposition is the right answer to the question.
 * @author bog
 * @see Quizz
 */
public class Question implements Iterable<String>
{
    private String text;
    private ArrayList<String> propositions;
    private Integer answer;
    
    /**
     * @param String text, text of the question.
     */
    public Question (String text)
    {
	this.text = text;
	this.propositions = new ArrayList<String>();
	this.answer = null;
    }

    /**
     * Add a proposition for this question.
     * @param String proposition, the text of the proposition.
     */
    public void addProposition (String proposition)
    {
	this.propositions.add(proposition);
    }

    @Override
    public Iterator<String> iterator ()
    {
	return this.propositions.listIterator();
    }
    
    @Override
    public String toString ()
    {
	return this.text;
    }

    public void removeProposition (int index)
    {
	this.propositions.remove(index);
    }
    
    /* Getters and setters */
    public String getText ()
    {
	return this.text;
    }

    public void setText (String text)
    {
	this.text = text;
    }

    public Integer getAnswer ()
    {
	if (this.answer == null)
	    {
		return null;
	    }
	
	Integer answer = new Integer( this.answer.intValue() );
	return answer;
    }

    public void setAnswer (Integer answer)
    {
	this.answer = answer;
    }
    
    public void setAnswer (int answer)
    {
	this.answer = new Integer(answer);
    }

    public String getProposition (int index)
    {
	return this.propositions.get(index);
    }

    public void setProposition (int index, String proposition)
    {
	this.propositions.set(index, proposition);
    }
    
    public ArrayList<String> getPropositions ()
    {
	return this.propositions;
    }
    
    /**
     * @return int, how many propositions the question have.
     */
    public int getPropositionNumber ()
    {
	return this.propositions.size();
    }
}

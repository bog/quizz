package ufrst.samp.bog.model;

// Android
import android.content.Context;
// Database
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class QuizDatabaseOpenHelper extends SQLiteOpenHelper
{
    public final static String DatabaseName = "quiz.db";
    public final static int DatabaseVersion = 1;
    // SQL code
    public final static String QuizTable = "quiz";
    public final static int QuizId = 0;
    public final static int QuizTitle = 1;
    public final static int QuizScore = 2;
    public final static String QuestionTable = "question";
    public final static int QuestionId = 0;
    public final static int QuestionQuiz = 1;
    public final static int QuestionText = 2;
    public final static String PropositionTable = "proposition";
    public final static int PropositionId = 0;
    public final static int PropositionQuestion = 1;
    public final static int PropositionText = 2;
    public final static int PropositionIsAnswer = 3;

    public final static String SQLCreateQuiz = "CREATE TABLE "
	+ QuizDatabaseOpenHelper.QuizTable
	+ " (id INTEGER PRIMARY KEY AUTOINCREMENT, title VARCHAR(128) NOT NULL, score INTEGER NOT NULL);";

    public final static String SQLCreateQuestion = "CREATE TABLE "
	+ QuizDatabaseOpenHelper.QuestionTable
	+ " (id INTEGER PRIMARY KEY AUTOINCREMENT, "
	+ QuizDatabaseOpenHelper.QuizTable
	+ " INTEGER, text VARCHAR(256) NOT NULL, FOREIGN KEY ("
	+ QuizDatabaseOpenHelper.QuizTable
	+ ") REFERENCES "
	+ QuizDatabaseOpenHelper.QuizTable
	+ "(id));";

    public final static String SQLCreateProposition = "CREATE TABLE "
	+ QuizDatabaseOpenHelper.PropositionTable
	+ " (id INTEGER PRIMARY KEY AUTOINCREMENT, "
	+ QuizDatabaseOpenHelper.QuestionTable
	+ " INTEGER, text VARCHAR(256) NOT NULL, isanswer BOOLEAN NOT NULL, FOREIGN KEY ("
	+ QuizDatabaseOpenHelper.QuestionTable
	+ ") REFERENCES "
	+ QuizDatabaseOpenHelper.QuestionTable
	+ "(id));";

    public final static String SQLDrop = "DROP TABLE proposition; DROP TABLE question; DROP TABLE quiz;";

    public QuizDatabaseOpenHelper (Context context)
    {
	super(context
	      , QuizDatabaseOpenHelper.DatabaseName
	      , null /* default cursor factory */
	      , QuizDatabaseOpenHelper.DatabaseVersion);
    }

    @Override
    public void onUpgrade (SQLiteDatabase db, int old_version, int new_version)
    {
	db.execSQL("DROP IF EXISTS " + QuizDatabaseOpenHelper.PropositionTable);
	db.execSQL("DROP IF EXISTS " + QuizDatabaseOpenHelper.QuestionTable);	
	db.execSQL("DROP IF EXISTS " + QuizDatabaseOpenHelper.QuizTable);
	
	onCreate(db);
    }

    @Override
    public void onCreate (SQLiteDatabase db)
    {
	db.execSQL(QuizDatabaseOpenHelper.SQLCreateQuiz);
	db.execSQL(QuizDatabaseOpenHelper.SQLCreateQuestion);
	db.execSQL(QuizDatabaseOpenHelper.SQLCreateProposition);
    }
}

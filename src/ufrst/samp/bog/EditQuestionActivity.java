package ufrst.samp.bog;

// Java
import java.util.ArrayList;
// Android
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.content.DialogInterface;
// Widget
import android.widget.Toast;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ArrayAdapter;
// MVC
import ufrst.samp.bog.model.*;

/**
 * Allow the user to edit a question.
 * @author bog
 */
public class EditQuestionActivity extends Activity
{
    private Question question;
    private ListView proposition_list;
    private EditText title_edit;
    
    @Override
    public void onCreate (Bundle bundle)
    {
	super.onCreate(bundle);
	setContentView(R.layout.editquestion);

	{
	    int index_quiz = getIntent().getIntExtra("quiz", 0);
	    int index_question = getIntent().getIntExtra("question", 0);
	    Quiz quiz = App.getInstance(this).getQuiz(index_quiz);
	    this.question = quiz.getQuestion(index_question);	    
	}

	this.title_edit = (EditText) findViewById(R.id.editQuestionTitle_edit);
	this.title_edit.setText( this.question.getText() );
    }

    @Override
    public void onResume ()
    {
	super.onResume();
	initializePropositionList();
    }

    private void initializePropositionList ()
    {
	this.proposition_list = (ListView) findViewById(R.id.proposition_list);

	ArrayList<String> propositions = new ArrayList<String>();

	int proposition_counter = 0;
	Integer answer = this.question.getAnswer();
	
	for (String proposition : this.question)
	    {
		String result = "";

		result += proposition;
		
		if ( answer != null
		     && proposition_counter == answer.intValue() )
		    {
			result += " (*)";
		    }
		
		propositions.add(result);
		proposition_counter++;
	    }
	
	ArrayAdapter<String> adapter = new ArrayAdapter<String>( this
								 , android.R.layout.simple_list_item_1
								 , propositions );
	
	this.proposition_list.setAdapter(adapter);

	this.proposition_list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
		@Override
		public void onItemClick (AdapterView adapter, View view, int position, long id)
		{
		    // Edition of a proposition.
		    AlertDialog.Builder adb = new AlertDialog.Builder(EditQuestionActivity.this);
		    final View dialog_view = EditQuestionActivity.this.getLayoutInflater().inflate(R.layout.createproposition, null);
		    final int proposition_id = position;
		    
		    adb.setTitle(R.string.createProposition_name);
		    adb.setMessage(R.string.createProposition_titleHint);
		    adb.setView(dialog_view);
		    adb.setIcon(R.drawable.ic_launcher);
		    
		    EditText text = (EditText) dialog_view.findViewById(R.id.createPropositionText_edit);
		    text.setText(EditQuestionActivity.this.question.getProposition(position));

		    CheckBox check = (CheckBox) dialog_view.findViewById(R.id.createPropositionAnswer_check);
		    
		    if (EditQuestionActivity.this.question.getAnswer() != null
			&& EditQuestionActivity.this.question.getAnswer().intValue() == proposition_id)
			{
			    check.setChecked(true);
			}
		    
		    adb.setPositiveButton(R.string.editButton, new DialogInterface.OnClickListener(){
			    @Override
			    public void onClick (DialogInterface dialog, int id)
			    {
				editProposition(proposition_id, dialog_view);
			    }
			});

		    adb.setNeutralButton(R.string.backButton, new DialogInterface.OnClickListener(){
			    @Override
			    public void onClick (DialogInterface dialog, int id)
			    {
				
			    }
			});

		    adb.setNegativeButton(R.string.deleteButton, new DialogInterface.OnClickListener(){
			    @Override
			    public void onClick (DialogInterface dialog, int id)
			    {
				EditQuestionActivity.this.question.removeProposition(proposition_id);
				initializePropositionList();
			    }
			});
		    
		    adb.show();
		}
	    });
    }
    
    public void onValidateQuestionButton (View view)
    {
	if (this.question.getAnswer() == null)
	    {
		Toast.makeText(this, R.string.editQuestion_noAnswerError, Toast.LENGTH_SHORT).show();
		return;
	    }

	EditText text = (EditText) findViewById(R.id.editQuestionTitle_edit);

	if ( text.getText().toString().equals("") )
	    {
		Toast.makeText(this, R.string.createQuestion_noTextError, Toast.LENGTH_SHORT).show();
		return;
	    }
	
	this.question.setText( text.getText().toString().trim() );
	
	finish();
    }

    public void onAddPropositionButton (View view)
    {
	AlertDialog.Builder adb = new AlertDialog.Builder(this);
	final View dialog_view = getLayoutInflater().inflate(R.layout.createproposition, null);

	adb.setTitle(R.string.createProposition_name);
	adb.setMessage(R.string.createProposition_titleHint);
	adb.setView(dialog_view);
	adb.setIcon(R.drawable.ic_launcher);
	
	adb.setPositiveButton(R.string.createProposition_createButton, new DialogInterface.OnClickListener(){
		@Override
		public void onClick (DialogInterface dialog, int id)
		{
		    createProposition(dialog_view);
		}
	    });

	adb.setNegativeButton(R.string.createProposition_backButton, new DialogInterface.OnClickListener(){
		@Override
		public void onClick (DialogInterface dialog, int id)
		{
		    
		}
	    });
	
	adb.show();
    }

    private void createProposition (View dialog_view)
    {
	int proposition_index = EditQuestionActivity.this.question.getPropositionNumber();
	EditQuestionActivity.this.question.addProposition("");
	
	editProposition(proposition_index, dialog_view);
    }

    private void editProposition (int id, View dialog_view)
    {
	EditText text = (EditText) dialog_view.findViewById(R.id.createPropositionText_edit);

	if ( text.getText().toString().equals("") )
	    {
		Toast.makeText(EditQuestionActivity.this, R.string.createProposition_noTextError, Toast.LENGTH_SHORT).show();
		return;
	    }

	EditQuestionActivity.this.question.setProposition( id, text.getText().toString().trim() );
	
	CheckBox check = (CheckBox) dialog_view.findViewById(R.id.createPropositionAnswer_check);
	
	if ( check.isChecked() )
	    {
		EditQuestionActivity.this.question.setAnswer(id);
	    }
	else if (EditQuestionActivity.this.question.getAnswer() != null
		 && EditQuestionActivity.this.question.getAnswer().intValue() == id)
	    {
		EditQuestionActivity.this.question.setAnswer(null);
	    }
		    
	initializePropositionList();
    }
}

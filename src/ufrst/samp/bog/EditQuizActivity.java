package ufrst.samp.bog;

// Android
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.content.DialogInterface;
// Widget
import android.widget.Toast;
import android.widget.EditText;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ArrayAdapter;
// MVC
import ufrst.samp.bog.model.*;

/**
 * Allow the user to edit a quiz.
 * @author bog
 */
public class EditQuizActivity extends Activity
{
    private Quiz quiz;
    private ListView question_list;
    
    @Override
    public void onCreate (Bundle bundle)
    {
	super.onCreate(bundle);
	setContentView(R.layout.editquiz);

	{
	    int index = getIntent().getIntExtra("quiz", 0);
	    this.quiz = App.getInstance(this).getQuiz(index);
	}

	EditText quiz_title = (EditText) findViewById(R.id.editQuizTitle_edit);
	quiz_title.setText( this.quiz.getTitle() );

	this.question_list = (ListView) findViewById(R.id.question_list);
    }

    @Override
    public void onResume ()
    {
	super.onResume();
	initializeQuestionList();
    }
    
    private void initializeQuestionList()
    {
	ArrayAdapter<Question> adapter = new ArrayAdapter<Question>( this
								    , android.R.layout.simple_list_item_1
								    , this.quiz.getAllQuestions() );
	this.question_list.setAdapter(adapter);

	this.question_list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
		@Override
		public void onItemClick (AdapterView adapter, View view, int position, long id)
		{
		    AlertDialog.Builder adb = new AlertDialog.Builder(EditQuizActivity.this);
		    final int question_index = position;
		    
		    adb.setTitle(R.string.editQuestion_name);
		    adb.setIcon(R.drawable.ic_launcher);
		    
		    adb.setPositiveButton(R.string.editButton, new DialogInterface.OnClickListener(){
			    @Override
			    public void onClick (DialogInterface dialog, int id)
			    {
				editQuestion( EditQuizActivity.this.quiz.getQuestion(question_index) );
			    }
			});

		    adb.setNeutralButton(R.string.backButton, new DialogInterface.OnClickListener(){
			    @Override
			    public void onClick (DialogInterface dialog, int id)
			    {
			    }
			});

		    adb.setNegativeButton(R.string.deleteButton, new DialogInterface.OnClickListener(){
			    @Override
			    public void onClick (DialogInterface dialog, int id)
			    {
				EditQuizActivity.this.quiz.removeQuestion(question_index);
				initializeQuestionList();
			    }
			});
		    
		    adb.show();
		}
	    });
    }
    
    public void onAddQuestionButton (View view)
    {
	final View dialog_view = getLayoutInflater().inflate(R.layout.createquestion, null);
	AlertDialog.Builder adb = new AlertDialog.Builder(this);

	adb.setTitle(R.string.createQuestion_name);
	adb.setMessage(R.string.createQuestion_help);
	adb.setView(dialog_view);
	adb.setIcon(R.drawable.ic_launcher);
	
	adb.setPositiveButton(R.string.createQuestion_createButton, new DialogInterface.OnClickListener(){
		@Override
		public void onClick(DialogInterface dialog, int id)
		{
		    // Adding the question to the quiz.
		    EditText createQuestion_edit = (EditText) dialog_view.findViewById(R.id.createQuestion_edit);

		    if ( createQuestion_edit.getText().toString().equals("") )
			{
			    Toast.makeText(EditQuizActivity.this, R.string.createQuestion_noTextError, Toast.LENGTH_SHORT).show();
			    return;
			}

		    Question question = new Question(createQuestion_edit.getText().toString().trim());
		    EditQuizActivity.this.quiz.addQuestion(question);

		    // Move to the edition of this question.
		    editQuestion(question);
		}
	    });

	adb.setNegativeButton(R.string.createQuestion_backButton, new DialogInterface.OnClickListener(){
		@Override
		public void onClick(DialogInterface dialog, int id)
		{
		    
		}
	    });

	adb.show();
    }

    public void onValidateQuizButton (View view)
    {
	if (this.quiz.getQuestionNumber() == 0)
	    {
		Toast.makeText(this, R.string.editQuiz_noQuestionError, Toast.LENGTH_SHORT).show();
		return;
	    }
	
	for (Question question : this.quiz)
	    {
		if (question.getAnswer() == null)
		    {
			Toast.makeText(this
				       , getResources().getString(R.string.editQuestion_noAnswerError)
				       + " "
				       + question.getText()
				       , Toast.LENGTH_SHORT).show();
			return;
		    }
	    }
	
	EditText title = (EditText) findViewById(R.id.editQuizTitle_edit);
	
	if ( title.getText().toString().equals("") )
	    {
		Toast.makeText(this, R.string.createQuiz_noTitleError, Toast.LENGTH_SHORT).show();
		return;
	    }

	this.quiz.setTitle( title.getText().toString().trim() );

	App.getInstance(this).saveQuiz(this.quiz);
	    
	finish();
    }

    /**
     * Start the question edition activity.
     * @param Question question, the question to edit.
     */
    private void editQuestion (Question question)
    {
	Intent intent = new Intent(this, EditQuestionActivity.class);
	intent.putExtra( "quiz", App.getInstance(this).indexOfQuiz(this.quiz) );
	intent.putExtra( "question", this.quiz.indexOfQuestion(question) );

	startActivity(intent);
    }
}
